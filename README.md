# Thiq
A JavaScript plugin first started at PlugJS. Now that it is outdated and not managed, I have decided to continue
it as a new plugin in itself.

[Original repo](https://github.com/connicpu/plugjs)